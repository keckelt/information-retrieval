package at.ac.tuwien.ir.group12.exercise2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

/**
 * Hello world!
 *
 */
public class App {
	private static final File index = new File("./index");
	private static final File newsgroups = new File("./20_newsgroups_subset");

	private static float delta;

	public static void main(String[] args) {
		System.out.println("Exercise 1 - Basic Search System");
		System.out.println("Group12");
		System.out.println("--------------------------------\n");

		if (args == null || args.length == 0) {
			System.err
					.println("You must provide a delta value for BM25LSimilarity");
			System.exit(-1);
		} else if (args[0].isEmpty()) {
			System.err
					.println("You must provide a delta value for BM25LSimilarity");
			System.exit(-1);
		} else if (!DoubleChecker.isDouble(args[0])) // is double value?
		{
			System.err.println(args[0] + " is not a number.");
			System.exit(-1);

		} else {
			delta = Float.parseFloat(args[0]);
		}
		
		Similarity simFunction = new BM25LSimilarity(1.2f, 0.75f, delta); //exercise impl
		//for report:
//		Similarity simFunction = new BM25Similarity(1.2f, 0.75f); //lucene impl
//		Similarity simFunction = new DefaultSimilarity(); //lucene default

		System.out.println("Delta = " + delta);
		System.out.println("Creating index, please wait.");
		try {
			IndexFiles.doIndex(newsgroups.getPath(), index.getPath(), false, simFunction);
		} catch (IOException e1) {
			System.err
					.println("Error accessing the doc or index directory. Exiting");
			System.err.println(e1.getMessage());
			System.exit(-1);
		}

		System.out.println("Index Created, you may search now:");
		System.out
				.println("Enter either the path to the topic file, or type exit to leave this application");

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));
		while (true) {
//		for(int i=1; i<=20;i++) { //4 report
			try {
				String userInput = reader.readLine();
//				String userInput = "topics/topic"+i; //4 report
				if (userInput.equals("exit")) {
					System.out.println("Bye");
					System.exit(0);
				} else if (!userInput.isEmpty()) {

					File topic = new File(userInput);
					if (!topic.exists() || !topic.isFile()) {
						System.err.println(userInput
								+ " doesn't exist or is not a file.");
						continue;
					} else if (!topic.canRead()) {
						System.err.println("Can't read given file.");
						continue;
					}

					try {
						ScoreDoc[] hits = SearchFiles.searchTopic(index, topic, simFunction);

						printScores(userInput, hits);

					} catch (FileNotFoundException e) {
						System.err.println("Directory not found; exiting");
						System.exit(-1);
					} catch (ParseException e) {
						System.err.println("Parsing errpr; exiting.");
						System.err.println(e.getMessage());
						System.exit(-1);
					} catch (IOException e) {
						System.err
								.println("Error accessing directory; exiting.");
						System.err.println(e.getMessage());
						System.exit(-1);
					}
				}

			} catch (Exception e1) {
				System.err.println("IO Error occured, sorry.\nMessage: "
						+ e1.getMessage());
			}

		}
	}

	private static void printScores(String topic, ScoreDoc[] hits) throws IOException {
		DirectoryReader ireader = DirectoryReader.open(FSDirectory.open(Paths
				.get(index.getPath())));
		IndexSearcher isearcher = new IndexSearcher(ireader);
//		BufferedWriter writer = new BufferedWriter(new FileWriter(new File ("Lucene_Default-Scores.txt"),true)); //4 report
		for (int i = 0; i < hits.length; i++) {
		String scoreLine = topic.replaceFirst(".*topics\\/", "")+" Q0 "
					+ isearcher.doc(hits[i].doc).get("path").replaceFirst(".*20_newsgroups_subset\\/", "")+ " " + (i + 1)
					+ " " + hits[i].score + " group12-experiment2";
		System.out.println(scoreLine);
//		writer.write(scoreLine+"\n"); //4 report
		}
//		writer.flush(); // 4 report
//		writer.close(); // 4 report
	}

}
