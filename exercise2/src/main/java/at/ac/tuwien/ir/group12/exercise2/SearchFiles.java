package at.ac.tuwien.ir.group12.exercise2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

public class SearchFiles {
	
	private SearchFiles()
	{
		
	}
	
	public static ScoreDoc[] searchTopic(File index, File topic, Similarity simFunction) throws FileNotFoundException, ParseException, IOException
	{
		DirectoryReader ireader = DirectoryReader.open(FSDirectory.open(Paths
				.get(index.getPath())));
		IndexSearcher isearcher = new IndexSearcher(ireader);
		isearcher.setSimilarity(simFunction);
		Analyzer analyzer2 = new StandardAnalyzer();
		QueryParser parser = new QueryParser("contents", analyzer2);

		// http://lucene.apache.org/core/5_0_0/queryparser/overview-summary.html
		// suggest this:
		// StandardQueryParser qpHelper = new StandardQueryParser(analyzer);
		// Query query = qpHelper.parse("contents",
		// QueryParser.escape(readAllLines(new File("./topics/topic1"))));
		// but that would search for all terms in the file

		Query query = parser.parse(QueryParser
				.escape(readAllLines(topic))); // escaping
																		// special
																		// characters
																		// is
																		// important!!!
		
		ScoreDoc[] scoreDocs = isearcher.search(query, null, 100).scoreDocs;
		
//		Explanation explain = isearcher.explain(query, scoreDocs[0].doc);
//		System.out.println(explain.toString());
//		explain = isearcher.explain(query, scoreDocs[1].doc);
//		System.out.println(explain.toString());
		return scoreDocs;
	}
	
	private static String readAllLines(File absoluteFilePath)
			throws FileNotFoundException, IOException {
		StringBuilder allLines = new StringBuilder();

		try (BufferedReader br = new BufferedReader(new FileReader(
				absoluteFilePath))) {
			String line;
			while ((line = br.readLine()) != null) {
				allLines.append(" " + line);
			}
		}

		return allLines.toString();
	}

}
