package at.ac.tuwien.ir.group12.exercise1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class App {

	static boolean caseFolding = false;
	static boolean stopWords = false;
	static boolean stemming = false;

	static Hashtable<String, List<String>> bow_index;
	static Hashtable<String, List<String>> biword_index;

	public static void main(String[] args) {
		System.out.println("Exercise 1 - Basic Search System");
		System.out.println("Group12");
		System.out.println("--------------------------------\n");

		// Vocabulary normalization
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("--case")) {
				System.out.println("Use case folding");
				caseFolding = true;
			} else if (args[i].equals("--stop")) {
				System.out.println("Remove stop words");
				stopWords = true;
			} else if (args[i].equals("--stem")) {
				System.out.println("Use stemming");
				stemming = true;
			} else {
				if (!args[i].equals("--help"))
					System.err.println("Unknown command: '" + args[i] + "'");

				printHelp();
				System.exit(-1);
			}
		}

		System.out.println("Creating Index...");
		createIndex(stopWords, caseFolding, stemming);

		System.out.println();
		System.out.println("Index Created, you may search now:");
		printSearchHelp();

		// Search Paremeters:
		// # Usage of bag-of-words or bi-gram index
		// # Scoring Method
		// open up standard input
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));
		
//		for(int i=1; i<=20; i++) {
		while (true) {
			try {
				String query = reader.readLine();
//				String query = "--bi --logtf topics/topic"+i;
				if (query.equals("exit")) {
					System.out.println("Bye");
					System.exit(0);
				} else if (!query.isEmpty()) {
					System.out.println("Query=" + query);
					String[] parameters = query.split(" ");

					if (parameters.length != 3) {
						System.err.println("You need to provide 3 parameters.");
						printSearchHelp();
						continue;
					}

					boolean bag;
					// index selection
					if (parameters[0].equals("--bag")) {
						bag = true;
					} else if (parameters[0].equals("--bi")) {
						bag = false;
					} else {
						System.err.println("Unknown 1st parameter: "
								+ parameters[0]);
						printSearchHelp();
						continue;
					}

					boolean tf;
					// scoring selection
					if (parameters[1].equals("--tf")) {
						tf = true;
					} else if (parameters[1].equals("--logtf")) {
						tf = false;
					} else {
						System.err.println("Unknown 2nd parameter: "
								+ parameters[1]);
						printSearchHelp();
						continue;
					}

					File topic = new File(parameters[2]);
					if (!topic.exists() || !topic.isFile()) {
						System.err.println(parameters[2]
								+ " is not a file or cannot be found");
						continue;
					}

					createResult(bag, tf, parameters[2]);
				}

			} catch (Exception e) {
				System.err.println("IO Error occured, sorry.\nMessage: "
						+ e.getMessage());
			}
		}
	}

	private static void printSearchHelp() {
		System.out.println("1st Parameter - Select Index:");
		System.out.println("\t--bag\tUse bag-of-words index, or");
		System.out.println("\t--bi\tUse bi-gram index.");
		System.out.println("2nd Parameter - Select Scoring Method:");
		System.out.println("\t--tf\tUse term frequency for scoring, or");
		System.out
				.println("\t--logtf\tUse logarithmic term frequency for scoring.");
		System.out.println("3d Parameter:");
		System.out.println("\tPath to topic file.");
		System.out.println("Or type:");
		System.out.println("\texit\tto leave application");
	}

	private static void printHelp() {
		System.out.println("Parameters (for vocabulary normalisation):");
		System.out.println("\t--case ... uses case folding");
		System.out.println("\t--stop ... removes stop words");
		System.out.println("\t--stem ... chop end of words");

		System.out.println();
	}

	public static void createIndex(boolean stopWords, boolean caseFolding,
			boolean stemming) {

		// hashtabele: key=word, data=Vector/list with filenames
		bow_index = new Hashtable<String, List<String>>();
		biword_index = new Hashtable<String, List<String>>();

		List<File> all = listf("./20_newsgroups_subset");
		for (File file : all) {
			if (!file.getAbsoluteFile().isDirectory()) {
				// z++;
				// System.out.println("Filenum.: "+z+"   "+file.getName());
				try {
					String fileContent = readAllLines(file.getAbsoluteFile());
					String filename = getFilename(file.getCanonicalPath());

					String[] words = fileContent.split("\\s+");
					for (int i = 0; i < words.length; i++) {
						// You may want to check for a non-word character before
						// blindly
						// performing a replacement
						// It may also be necessary to adjust the character
						// class

						words[i] = words[i].replaceAll("[^\\w]", "");
						String word = words[i];

						if (word != "") {
							// create words-of-bag index
							List<String> newlist;
							if (bow_index.containsKey(word)) {
								// word exist -> old + current value have to be
								// combined
								newlist = bow_index.get(word);
								newlist.add(filename);

							} else {
								// word has to be added -> only value is the
								// current one
								newlist = new ArrayList<String>();
								newlist.add(filename);
							}

							// update(add or change) index
							bow_index.put(word, newlist);
						

						// Create Bi-Word Index
						int j = i + 1;
						while (j < words.length
								&& words[j].replaceAll("[^\\w]", "").isEmpty()) {
							j++; // Skip white space 'words'
						}

						if (j < words.length) {
							// Add current and next (real) word to bi-word index
							String biword = word + " "
									+ words[j].replaceAll("[^\\w]", "");

							List<String> newlist2;
							if (bow_index.containsKey(biword)) {
								// word exist -> old + current value have to be
								// combined
								newlist2 = biword_index.get(biword);
								newlist2.add(filename);

							} else {
								// word has to be added -> only value is the
								// current one
								newlist2 = new ArrayList<String>();
								newlist2.add(filename);
							}

							// update(add or change) index
							biword_index.put(biword, newlist2);

						}
						
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		System.out.println("size of bow_index: " + bow_index.size());
		System.out.println("size of biword_index: " + biword_index.size());

	}

	private static String getFilename(String path) {
		String path_seg[] = path.split(Pattern.quote(File.separator));

		int size = path_seg.length;

		String filename = path_seg[size - 2] + "\\" + path_seg[size - 1];

		return filename;
	}

	private static List<String> readTopic(String filename, boolean bag) {

		File file = new File(filename);

		// search words
		List<String> search = new ArrayList<String>();

		String fileContent;
		try {
			fileContent = readAllLines(file);

			String[] words = fileContent.split("\\s+");

			for (int i = 0; i < words.length; i++) {
				// You may want to check for a non-word character before
				// blindly
				// performing a replacement
				// It may also be necessary to adjust the character
				// class

				words[i] = words[i].replaceAll("[^\\w]", "");
				String word = words[i];

				if (word != "") {

					if (!bag) {
						// Create Bi-Word Index
						int j = i + 1;
						while (j < words.length
								&& words[j].replaceAll("[^\\w]", "").isEmpty()) {
							j++; // Skip white space 'words'
						}

						if (j < words.length) {
							// Add current and next (real) word to bi-word index
							word = word + " "
									+ words[j].replaceAll("[^\\w]", "");

						}
					}

					if (!search.contains(word)) {
						search.add(word);
					}

				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return search;
	}

	private static String readAllLines(File absoluteFilePath)
			throws FileNotFoundException, IOException {
		StringBuilder allLines = new StringBuilder();

		try (BufferedReader br = new BufferedReader(new FileReader(
				absoluteFilePath))) {
			String line;
			while ((line = br.readLine()) != null) {
				allLines.append(" " + line);
			}
		}

		return Normalize(allLines.toString());
	}

	private static String Normalize(String text) {

		if (caseFolding) {
			text = text.toLowerCase();
		}

		if (stopWords) {
			Pattern stopWords = Pattern
					.compile("\\b(a|an|and|are|as|at|be|by|for|from|has|he|in|is|it|its|of|on|that|the|to|was|were|will|with)\\b"); // \\b=
																																	// word
																																	// boundry
			Matcher matcher = stopWords.matcher(text);
			text = matcher.replaceAll("");
		}

		if (stemming) {
			Pattern suffixes = Pattern.compile("(s|'s)\\b");
			Matcher matcher = suffixes.matcher(text);
			text = matcher.replaceAll("");
		}

		return text;
	}

	private static void createResult(boolean bag,
			boolean tf, String topic) {
		
		List<String> topicWords = readTopic(topic,bag);
		Hashtable<String, List<String>> index;
		Hashtable<String, Double> scores = new Hashtable<>();
		if (bag) {
			index = bow_index;
		} else {
			index = biword_index;
		}

		for (String word : topicWords) {
			if (index.containsKey(word)) {

				// Compute term frequency for this word:
				Hashtable<String, Integer> wordScore = new Hashtable<>();

				List<String> list = index.get(word);
				for (String fileWithWord : list) {
					if (wordScore.containsKey(fileWithWord)) {
						wordScore.put(fileWithWord,
								wordScore.get(fileWithWord) + 1);
					} else {
						wordScore.put(fileWithWord, new Integer(1));
					}
				}

				// Sum up term frequencies of all words
				Iterator<String> keyIterator = wordScore.keySet().iterator();
				while (keyIterator.hasNext()) {
					String key = keyIterator.next();

					// Compute tf or logtf
					double score = wordScore.get(key);
					if (!tf) {
						if (score <= 0) {
							score = 0; // set to 0 instead of negative infinity
						} else {
							score = 1 + Math.log(score);
						}
					}

					if (scores.containsKey(key)) {
						scores.put(key, scores.get(key) + score);
					} else {
						scores.put(key, score);
					}
				}
			}
		}

		// http://stackoverflow.com/a/23846961/2549748
		 Stream<Entry<String, Double>> topHundred = scores.entrySet().stream()
				.sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
				.limit(100);
		 
		 Iterator<Entry<String, Double>> iterator = topHundred
					.iterator();
		 
//		 try {
//			BufferedWriter writer = new BufferedWriter(new FileWriter("Ex1-biword_index.txt",true));
		
		
		int i = 1;
		while (iterator.hasNext()) {
			Entry<String, Double> entry = iterator.next();
			String output = (new File(topic).getName() + " ");
			output += ("Q0 ");
			output += (entry.getKey() + " ");
			output += (i + " ");
			output += (entry.getValue() + " ");
			output += ("group12-experiment1-bi-");
			if (tf) {
				output += ("tf");
			} else {
				output += ("logtf");
			}
			System.out.println(output);
//			writer.write(output+"\n");
			i++;
		}
		
//		writer.flush();
//		writer.close();
		
//		 } catch (IOException e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
	}

	public static List<File> listf(String directoryName) {
		File directory = new File(directoryName);

		List<File> resultList = new ArrayList<File>();

		// get all the files from a directory
		File[] fList = directory.listFiles();
		resultList.addAll(Arrays.asList(fList));
		for (File file : fList) {
			if (file.isFile()) {
				// System.out.println(file.getAbsolutePath());
			} else if (file.isDirectory()) {
				resultList.addAll(listf(file.getAbsolutePath()));
			}
		}
		// System.out.println(fList);
		return resultList;
	}
}
